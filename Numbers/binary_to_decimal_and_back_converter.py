#! python3
import unittest


class BinaryToAndFromDecimal:
    def __init__(self):
        pass

    def toBinary(self, num):
        return int(bin(num)[2:])

    def toDecimal(self, binnum):
        return int(str(binnum), 2)


class MyTest(unittest.TestCase):
    def test_me(self):
        b = BinaryToAndFromDecimal()
        self.assertEqual(b.toBinary(15), (1111))  # OK
        self.assertEqual(b.toDecimal(1001), (9))  # OK


unittest.main()
