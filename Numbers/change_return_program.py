#! python3
import unittest
from random import randint


class ChangeReturn:
    def __init__(self):
        self.quarter = .25
        self.nickel = .05
        self.dime = .10
        self.penny = .01
        self.dollar1 = 1.00
        self.dollar5 = 5.00
        self.dollar10 = 10.00
        self.dollar20 = 20.00
        self.quarteramount = 0
        self.nickelamount = 0
        self.dimeamount = 0
        self.pennyamount = 0
        self.dollar1amount = 0
        self.dollar5amount = 0
        self.dollar10amount = 0
        self.dollar20amount = 0

    def returnChange(self, total, paid):
        total = float(total)
        paid = float(paid)
        change = paid - total
        changeback = change

        while change - self.dollar20 >= 0:
            self.dollar20amount += 1
            change -= 20.00
        while change - self.dollar10 >= 0:
            self.dollar10amount += 1
            change -= 10.00
        while change - self.dollar5 >= 0:
            self.dollar5amount += 1
            change -= 5.00
        while change - self.dollar1 >= 0:
            self.dollar1amount += 1
            change -= 1.00
        while change - self.quarter >= 0:
            self.quarteramount += 1
            change -= .25
        while change - self.dime >= 0:
            self.dimeamount += 1
            change -= .10
        while change - self.nickel >= 0:
            self.nickelamount += 1
            change -= .05
        while change - self.penny >= 0:
            self.pennyamount += 1
            change -= .01

        cashback = "$20: %s\n$10: %s\n$5: %s\n$1: %s\nQuarters: %s\nDimes: %s\nNickels: %s\nPennies: %s" % (
            self.dollar20amount, self.dollar10amount, self.dollar5amount, self.dollar1amount, self.quarteramount, self.dimeamount, self.nickelamount, self.pennyamount)

        return changeback, cashback


class MyTest(unittest.TestCase):
    def test_me(self):
        c = ChangeReturn()
        self.assertEqual(c.returnChange(6.71, 20), (13.29,
                                                    '$20: 0\n$10: 1\n$5: 0\n$1: 3\nQuarters: 1\nDimes: 0\nNickels: 0\nPennies: 3'))  # OK


unittest.main()
