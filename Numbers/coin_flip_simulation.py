#! python3
import unittest
from random import randint


class CoinFlip:
    def __init__(self):
        pass

    def flipCoin(self, numberoftimes):
        numberoftimes = int(numberoftimes)
        heads = 0
        tails = 0
        for num in range(numberoftimes):
            headsortails = randint(0, 1)
            if headsortails == 0:
                heads += 1
            else:
                tails += 1

        if range(numberoftimes) == 1:
            if heads != []:
                return 'Heads'
            else:
                return 'Tails'
        else:
            return "Heads: %s, Tails: %s" % (heads, tails)


class MyTest(unittest.TestCase):
    def test_me(self):
        c = CoinFlip()
        self.assertRegexpMatches(c.flipCoin('1'), ('([A-Z])\w+'))  # 'Heads' or 'Tails', OK
        self.assertRegexpMatches(c.flipCoin('15'), ('(Heads: )\d+(, Tails: )\d+'))  # 'Heads: 5, Tails: 10', OK


unittest.main()
