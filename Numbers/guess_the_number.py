#! python3
import unittest
from random import randint
import msvcrt as wait


class GuessTheNumber:
    def __init__(self):
        pass

    def computerGuesses(self, yournumber, nummin, nummax):
        yournumber = int(yournumber)
        nummin = int(nummin)
        nummax = int(nummax)
        guesslist = []
        while True:
            guess = randint(nummin, nummax)
            while guess in guesslist:
                guess = randint(nummin, nummax)
            guesslist.append(guess)
            if yournumber == guess:
                print("\n\nThe computer correctly guessed your number")
                return int(guess)
            else:
                print("\n\n\nThe computer failed to guess your number\n\nPress any key to continue")
                wait.getch()

    def userGuesses(self, nummin, nummax):
        nummin = int(nummin)
        nummax = int(nummax)
        number = randint(nummin, nummax)
        while True:
            guess = input()
            if int(guess) == number:
                print("\n\nYou correctly guessed the computer's number")
                return int(guess)
            else:
                print("\n\nTry again")

    def playGame(self):
        print("Do you want to guess?")
        inputy = input()
        inputy = inputy.lower()
        if inputy == 'y' or inputy == 'yes':
            print("\nInput the minimum number the computer can pick")
            nummin = input()
            print("\nInput the maximum number the computer can pick")
            nummax = input()
            print("\nPick your number")
            GuessTheNumber().userGuesses(nummin, nummax)
        if inputy == 'n' or inputy == 'no':
            print("\nPick your number")
            yournumber = input()
            print("\nInput the minimum number the computer can guess from")
            nummin = input()
            print("\nInput the maximum number the computer can guess from")
            nummax = input()
            GuessTheNumber().computerGuesses(yournumber, nummin, nummax)


class MyTest(unittest.TestCase):
    def test_me(self):
        g = GuessTheNumber()
        print("\n\nGuess the computer's number")
        self.assertAlmostEqual(g.userGuesses(0, 4), (0), delta=5)  # OK
        print("\n\n\n\nPress any key")
        wait.getch()
        self.assertAlmostEqual(g.computerGuesses(6, 0, 9), (0), delta=10)  # OK


unittest.main()
GuessTheNumber().playGame()
