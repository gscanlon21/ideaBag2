#! python3
import unittest


class String:
    def __init__(self, string):
        self.string = string

    def countWordsInAString(self):
        string = self.string.split(' ')
        for ite in string:
            if '.' in ite:
                item = string.pop(string.index(ite))
                itemlist = item.split('.')
                for theitem in itemlist:
                    string.append(theitem)
            if ite == '':
                del string[(string.index(''))]

        return len(string)

    def countParagraphsInAString(self):
        if '\n\t' in self.string:
            string = self.string.split('\n\t')
        else:
            string = ['']

        return len(string)


class MyTest(unittest.TestCase):
    def test_me(self):
        s = String('The apple fell on top of the orange.\n\n\tIt is not okay. But it will be alright.')
        self.assertEqual(s.countWordsInAString(), (17))  # OK
        self.assertEqual(s.countParagraphsInAString(), (2))  # OK
        s = String('No')
        self.assertEqual(s.countWordsInAString(), (1))  # OK
        self.assertEqual(s.countParagraphsInAString(), (1))  # OK


unittest.main()
