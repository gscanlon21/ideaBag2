#! python3
import unittest


class String:
    def __init__(self, string):
        self.string = string

    def disemVoweler(self):
        string = self.string
        vowellist = [' ', 'a', 'e', 'i', 'o', 'u']
        vowelstring = ' '
        numberofvowels = 0
        for num in range(len(string)):
            num = num - numberofvowels
            if string[num].lower() in vowellist:
                vowel = string[num]
                string = string.replace(string[num], '', 1)
                numberofvowels += 1
                if vowel != ' ':
                    vowelstring += vowel

        return string.lower() + vowelstring.lower()


class MyTest(unittest.TestCase):
    def test_me(self):
        s = String('The quick brown fox jumped over the dog')
        self.assertEqual(s.disemVoweler(), ('thqckbrwnfxjmpdvrthdg euiooueoeeo'))  # OK
        s = String('Hello World')
        self.assertEqual(s.disemVoweler(), ('hllwrld eoo'))  # OK


unittest.main()
