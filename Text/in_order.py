#! python3
import unittest


class String:
    def __init__(self):
        pass

    def inOrder(self, stringinput):
        stringinput = stringinput.lower()
        index = 0
        previousindex = 0
        alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
                    'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
        for num, letter in enumerate(stringinput):
            index = alphabet.index(letter)
            if index >= previousindex:
                next
            else:
                return ('Not in order')
            previousindex = index

        return ('In order')


class MyTest(unittest.TestCase):
    def test_me(self):
        s = String()
        self.assertEqual(s.inOrder('Apples'), ('Not in order'))  # OK
        self.assertEqual(s.inOrder('abc'), ('In order'))  # OK


unittest.main()
